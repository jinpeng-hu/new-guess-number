import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class NewGuessNumberTest {
    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @Test
    void generateAnswerTest() {
        String str = new NewGuessNumber().generateAnswer();
        System.out.println(str);
        int one = Integer.parseInt(str.substring(0,1));
        int two = Integer.parseInt(str.substring(1,2));
        int three = Integer.parseInt(str.substring(2,3));
        int four = Integer.parseInt(str.substring(3,4));
        Assert.assertEquals(4, str.length());//是否是四个数字
        //判断是否有重复数字
        Assert.assertNotEquals(one, two);
        Assert.assertNotEquals(one, three);
        Assert.assertNotEquals(one, four);
        Assert.assertNotEquals(two, three);
        Assert.assertNotEquals(two, four);
        Assert.assertNotEquals(three, four);
        //判断是否是1到9之间

        Assert.assertTrue(one>=0&&one<=9);
        Assert.assertTrue(two>=0&&two<=9);
        Assert.assertTrue(three>=0&&three<=9);
        Assert.assertTrue(four>=0&&four<=9);
    }
    @Test
    void compareGuessAnswerTest(){
        NewGuessNumber nGuessNumber= new NewGuessNumber();
        Assert.assertEquals("", nGuessNumber.compareGuessAnswer(1432+"",1234+"") );
    }
    @Test
    void isWinTest(){
        NewGuessNumber nGuessNumber= new NewGuessNumber();
        nGuessNumber.compareGuessAnswer(1234+"",1234+"");
        Assert.assertTrue(nGuessNumber.isWin());
    }
}
