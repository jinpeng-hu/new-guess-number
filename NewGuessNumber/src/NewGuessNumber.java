import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class NewGuessNumber extends JFrame implements ActionListener {
    private int a = 0, b = 0;
    JButton btn_ok;JButton btn_cancel;JButton btn_jx;
    JLabel xh; JLabel xx; int TYPE = 10; String random = generateAnswer();
    JTextField user;
    JLabel xb;
    public NewGuessNumber() {
        super("猜数字2");
        System.out.println(random);
        JPanel pan = new JPanel();
        Container con = getContentPane();
        Dimension scr = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int) scr.getWidth();
        int height = (int) scr.getHeight();
        btn_ok = new JButton("确认");
        btn_cancel = new JButton("退出");
        btn_jx = new JButton("继续");
        user = new JTextField(20);
        xh = new JLabel("请输入4个0-9之间的整数:");
        xb = new JLabel("剩余次数:" + TYPE);
        xx = new JLabel("游戏玩法:猜对四个数字和他们的数字即可获胜");
        pan.add(xh);
        pan.add(xb);
        pan.add(user);
        pan.add(btn_ok);
        pan.add(btn_jx);
        pan.add(btn_cancel);
        pan.add(xx);
        setLocation((width - 300) / 2, (height - 150) / 2);
        btn_ok.addActionListener(this);
        btn_cancel.addActionListener(this);
        btn_jx.addActionListener(this);
        con.add(pan);
        btn_jx.setEnabled(false);
        setResizable(false);
        setSize(300, 145);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new NewGuessNumber();
    }
    public String generateAnswer() {
        StringBuilder str = new StringBuilder();
        while (str.length() < 4) {
            int r = new Random().nextInt(9);
            if (!str.toString().contains(r + "")) {
                str.append(r);
            }
        }
        return str.toString();
    }
    public String compareGuessAnswer(String r, String user) {
        a = b = 0;
        char[] r1 = r.toCharArray();
        char[] user1 = user.toCharArray();
        for (int i = 0; i < r1.length; i++) {
            if (r1[i] == user1[i]) {
                a++;
            } else {
                for (int j = 0; j < user1.length; j++) {
                    if (r1[i] == user1[j]) {
                        b++;
                    }
                }
            }


        }
        return a + "A" + b + "B";
    }

    public boolean isWin() {
        return a == 4;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (arg0.getSource() == btn_ok) {
            char []c = user.getText().toString().trim().toCharArray();
            int p =0;
            for (char ch:c){
                if (ch >= 9 || ch <= 0){
                    p++;
                }
            }
            if (p!=4) {
                xh.setText("您输入的数字不正确");
            } else {
                TYPE--;
                xb.setText("剩余次数:" + TYPE);
                if (TYPE <= 0) {
                    user.setText(null);
                    xh.setText("您没有猜对,游戏结束");
                    btn_jx.setEnabled(true);
                } else {

                    xh.setText(compareGuessAnswer(random,user.getText().toString().trim()));
                    user.setText(null);
                    if (isWin()){
                        xh.setText("您猜对了,游戏结束");
                        btn_jx.setEnabled(true);
                    }

                }
            }

        }
        if (arg0.getSource() == btn_cancel) {
            setVisible(false);
            dispose();
            System.exit(0);
        }
        if (arg0.getSource() == btn_jx) {
            TYPE = 10;
            random=generateAnswer();
            xh.setText("请输入4个0-9之间的整数:");
            xb.setText("剩余次数:" + TYPE);
            user.setText(null);
        }
    }
}
